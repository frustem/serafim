/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === 'MarkdownRemark') {
    const slug = createFilePath({node, getNode, basePath: 'pages'})
    createNodeField({
      node,
      name: 'slug',
      value: slug,
    })
  }
}


exports.createPages = async({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      shrines: allMarkdownRemark(filter: {frontmatter: {}, fields: {slug: { glob: "/content/shrines/**" }}}) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
      saints: allMarkdownRemark(filter: {frontmatter: {}, fields: {slug: { glob: "/content/saints/**" }}}) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
      books: allMarkdownRemark(filter: {frontmatter: {}, fields: {slug: { glob: "/content/books/**" }}}) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
      prayers: allMarkdownRemark(filter: {frontmatter: {}, fields: {slug: { glob: "/content/prayers/**" }}}) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
     news: allMarkdownRemark(filter: {frontmatter: {}, fields: {slug: { glob: "/content/news/**" }}}) {
      edges {
        node {
          fields {
            slug
          }
        }
      }
    }
    }
  `)
  result.data.shrines.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/article.js'),
      context: {
        slug: node.fields.slug,
      }
    })
  })
  
  result.data.saints.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/article.js'),
      context: {
        slug: node.fields.slug,
      }
    })
  })
  
  result.data.books.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/article.js'),
      context: {
        slug: node.fields.slug,
      }
    })
  })
  
  result.data.prayers.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/article.js'),
      context: {
        slug: node.fields.slug,
      }
    })
  })
  
  result.data.news.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('./src/templates/news.js'),
      context: {
        slug: node.fields.slug,
      }
    })
  })
  
}

